from django.db import models
from django.conf import settings
from datetime import datetime

class Owner(models.Model):
    class Meta:
        verbose_name = 'Владелец'
        verbose_name_plural = 'Владелцы'
    
    name_owner = models.CharField(verbose_name='Имя',max_length=32, blank=True, null=True)
    employees = models.PositiveIntegerField(verbose_name='Сотрудники',default=0)

    def __str__(self):
        return self.name_owner



class Position(models.Model):

    class Meta:
        verbose_name = 'Должность'
        verbose_name_plural = 'Должности'

    title = models.CharField(max_length=64, verbose_name='Должность', blank=True, null=True)

    def __str__(self):
        return self.title



class PlaseWork(models.Model):

    class Meta:
        verbose_name = 'Место работы'
        verbose_name_plural = 'Места работы'

    title = models.CharField(max_length=64, verbose_name='Место работы', blank=True, null=True)

    def __str__(self):
        return self.title



class WorkMan(models.Model):
    class Meta:
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'
        ordering = ['-id']

    # works = 'Работает'
    # fired = 'Уволен'
    # left_work = 'Уволился(фсь) по своим причинам'
    STATUS_CHOICES = [
        ('Работает', 'Работает'),
        ('Уволен(а)', 'Уволен(а)'),
        ('Уволился(ась) по своим причинам', 'Уволился(ась) по своим причинам')
    ]

    # higher = 'Высшее'
    # secondary = 'Среднее' 
    # elementary = 'Начальное'
    EDUCATION_CHOICES = [
        ('Высшее', 'Высшее'),
        ('Среднее' , 'Среднее'),
        ('Начальное', 'Начальное')
    ]

    owner_workman = models.ForeignKey(Owner,on_delete=models.CASCADE,blank=True,null=True,verbose_name='работодатель ')
    name = models.CharField(verbose_name='Имя',max_length=32)
    surname = models.CharField(verbose_name='Фамилия',max_length=32)
    patronymic = models.CharField(verbose_name='Отчество',max_length=32)
    dob = models.DateField(verbose_name='День рождение', blank=True, null=True)
    nation = models.CharField(verbose_name='Нация',max_length=32)
    contact_number = models.CharField(verbose_name='Контактный телефон', blank=True, null=True, max_length=13)
    email = models.CharField(verbose_name='Эдектронная почта',max_length=32)
    salary = models.PositiveIntegerField(verbose_name='Зарплата', default=0)
    education = models.CharField(verbose_name='Образование', max_length=64, blank=True, null=True, choices=EDUCATION_CHOICES)
    status = models.CharField(verbose_name='Статус', max_length=64, blank=True, null=True, choices=STATUS_CHOICES)
    image = models.ImageField(verbose_name='Изображение', upload_to='work_man_image/')
    position = models.ForeignKey(Position,on_delete=models.CASCADE, verbose_name='Должность', blank=True, null=True)
    plasework = models.ManyToManyField(PlaseWork, verbose_name='Место работы', blank=True, null=True)
    date_work = models.DateTimeField(verbose_name='Работает с ', default=datetime.now(), blank=True, null=True)


    def __str__(self):
        return self.name


class PreviousJob(models.Model):

    class Meta:
        verbose_name = 'Прежняя работа'
        verbose_name_plural = 'Прежнии работы'

    previous_job = models.ForeignKey(WorkMan,on_delete=models.CASCADE, verbose_name='Прежняя Работа', blank=True, null=True, related_name="workman_previous_job")
    no = models.CharField(verbose_name='Название организации',max_length=32)
    previous_position = models.ForeignKey(Position,on_delete=models.CASCADE, verbose_name='Должность', blank=True, null=True)
    work_period = models.TextField(verbose_name='Период работы',max_length=32)

    def __str__(self):
        return self.no