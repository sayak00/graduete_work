from django.urls import path
from data_base.views import OwnerView, PositionView, PlaseWorkView, WorkManView, PreviousJobView, WorkManDetailView


urlpatterns =[
    path('owner/',OwnerView.as_view({'get':'list','post':'create'})),
    path('position/',PositionView.as_view({'get':'list','post':'create'})),
    path('position/<int:pk>/', PositionView.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    path('plasework/',PlaseWorkView.as_view({'get':'list','post':'create'})),
    path('plasework/<int:pk>/', PlaseWorkView.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    path('owner/workman',WorkManView.as_view({'get':'list','post':'create'})),
    path('owner/workman/<int:pk>/',WorkManDetailView.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    path('owner/workman/previousjob',PreviousJobView.as_view({'get':'list','post':'create'})),
    path('owner/workman//previousjob/<int:pk>/',PreviousJobView.as_view({'get':'retrieve','put':'update','delete':'destroy'})),
    # path('owner/my/', PersonView.as_view({'get': 'my_workman'})),
]