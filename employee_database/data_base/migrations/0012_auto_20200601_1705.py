# Generated by Django 3.0.6 on 2020-06-01 11:05

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data_base', '0011_auto_20200601_1611'),
    ]

    operations = [
        migrations.AddField(
            model_name='workman',
            name='salary',
            field=models.PositiveIntegerField(default=0, verbose_name='Зарплата'),
        ),
        migrations.AlterField(
            model_name='workman',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2020, 6, 1, 17, 5, 19, 547665), null=True, verbose_name='Работает с '),
        ),
    ]
