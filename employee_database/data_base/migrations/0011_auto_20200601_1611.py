# Generated by Django 3.0.6 on 2020-06-01 10:11

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data_base', '0010_auto_20200531_2109'),
    ]

    operations = [
        migrations.AlterField(
            model_name='workman',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2020, 6, 1, 16, 11, 10, 197653), null=True, verbose_name='Работает с '),
        ),
    ]
