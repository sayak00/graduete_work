from django.shortcuts import render
from datetime import datetime
import django_filters.rest_framework
from rest_framework.viewsets import ModelViewSet
from rest_framework import filters,status
from data_base.models import Owner, Position, PlaseWork, WorkMan, PreviousJob
from data_base.serializers import OwnerSerializers, PositionSerializers, PlaseWorkSerializers, WorkManSerializers, PreviousJobSerializers, WorkManDetailSerializers
from rest_framework.response import Response
from collections import OrderedDict
from rest_framework.pagination import PageNumberPagination


class Pagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 200

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('page_count', self.page.paginator.num_pages),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data),
        ]))


class OwnerView(ModelViewSet):
    serializer_class = OwnerSerializers
    queryset = Owner.objects.all()
    lookup_field = 'pk'



class PositionView(ModelViewSet):
    serializer_class = PositionSerializers
    queryset = Position.objects.all()
    pagination_class = Pagination
    search_fields = ('title',)
    lookup_field = 'pk'



class PlaseWorkView(ModelViewSet):
    serializer_class= PlaseWorkSerializers
    queryset = PlaseWork.objects.all()
    pagination_class = Pagination
    search_fields = ('title',)
    lookup_field = 'pk'


class WorkManView(ModelViewSet):
    serializer_class = WorkManSerializers
    pagination_class = Pagination
    filter_backends = [filters.SearchFilter]
    search_fields = ('name','surname','dob','salary','age',
     'position', 'plasework', 'date_work', 'nation', 'education', 'status',
     'contact_number' 
    )
    lookup_field = 'pk'


    def get_queryset(self):
        queryset = WorkMan.objects.all()
        order_field = self.request.GET.get('order')
        filter_field = {}

        if self.request.GET.get('name'):
            filter_field['name'] = self.request.GET.get('name')

        if self.request.GET.get('surname'):
            filter_field['surname'] = self.request.GET.get('surname')

        if self.request.GET.get('dob'):
            filter_field['dob'] = self.request.GET.get('dob')

        if self.request.GET.get('salary'):
            filter_field['salary'] = self.request.GET.get('salary')

        # if self.request.GET.get('age'):
        #     if age:
        #         result = age - datetime.now().date().year
        #         if:
        #             result==dob.year
        #             elif datetime.now().date().month == dob.month:
        #                 if datetime.now().date().day < obj.dob.day:
        #         return
        #     filter_field['age'] = self.request.GET.get('age')

        if self.request.GET.get('nation'):
            filter_field['nation'] = self.request.GET.get('nation')

        if self.request.GET.get('position'):
            filter_field['position'] = self.request.GET.get('position')

        if self.request.GET.get('plasework'):
            filter_field['plasework'] = self.request.GET.get('plasework')

        if self.request.GET.get('date_work'):
            filter_field['date_work'] = self.request.GET.get('date_work')

        if self.request.GET.get('contact_number'):
            filter_field['contact_number'] = self.request.GET.get('contact_number')

        if self.request.GET.get('education'):
            filter_field['education'] = self.request.GET.get('education')

        if self.request.GET.get('status'):
            filter_field['status'] = self.request.GET.get('status')

        # if self.request.GET.get('work_period_workman'):
        #     filter_field['work_period_workman'] = self.request.GET.get('work_period_workman')

        if order_field:
            queryset = queryset.order_by(order_field)

        if filter_field:
            queryset = queryset.filter(**filter_field)

        start_date = self.request.GET.get('start_date')
        end_date = self.request.GET.get('end_date')

        if start_date and end_date:
            start_date = datetime.strptime('start_date', '%d.%m.%Y')
            end_date = datetime.strptime('end_date', '%d.%m.%Y')
            queryset = queryset.filter(date__date__gte=start_date, date__date__lte=end_date)

        return queryset


class PreviousJobView(ModelViewSet):
    serializer_class = PreviousJobSerializers
    queryset = PreviousJob.objects.all()
    pagination_class = Pagination
    lookup_field = 'pk'



class WorkManDetailView(ModelViewSet):
    serializer_class = WorkManDetailSerializers
    queryset = WorkMan.objects.all()
    lookup_field = 'pk'
    