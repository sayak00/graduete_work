from rest_framework import serializers
from data_base.models import Owner, Position, PlaseWork, WorkMan, PreviousJob
from datetime import datetime, timedelta



class OwnerSerializers(serializers.ModelSerializer):

    class Meta:
        model = Owner
        fields = ('employees', 'id')


class PositionSerializers(serializers.ModelSerializer):

    class Meta:
        model = Position
        fields = ('title', 'id')


class PlaseWorkSerializers(serializers.ModelSerializer):
    class Meta:
        model = PlaseWork
        fields = ('title', 'id')


class WorkManSerializers(serializers.ModelSerializer):
    owner_name = serializers.SerializerMethodField()
    position_name = serializers.SerializerMethodField()
    plasework_name = serializers.SerializerMethodField()
    date_work = serializers.DateTimeField(read_only=True, format='%d.%m.%Y')
    age = serializers.SerializerMethodField()
    work_period_workman = serializers.SerializerMethodField()

    class Meta:
        model = WorkMan
        fields = ('owner_workman', 'owner_name', 'name', 'surname', 'patronymic',
         'dob','salary' , 'nation', 'age', 'contact_number', 'email', 'image',
         'education', 'status', 'position', 'position_name', 'plasework', 'plasework_name', 'date_work',
         'work_period_workman', 'id'
        )

    def get_work_period_workman(self, obj):
        result = datetime.now().date() - obj.date_work.date()
        return result

    def get_age(self, obj):
        result = datetime.now().date().year - obj.dob.year
        if datetime.now().date().month < obj.dob.month:
            result -= 1
        elif datetime.now().date().month == obj.dob.month:
            if datetime.now().date().day < obj.dob.day:
                result -= 1
        print(result)
        return result

    def get_owner_name(self, obj):
        return obj.owner_workman.name_owner

    def get_position_name(self, obj):
        return obj.position.title

    def get_plasework_name(self, obj):
        return[{'id':item.id, 'name':item.title}for item in obj.plasework.all()]


class PreviousJobSerializers(serializers.ModelSerializer):
    previous_position_name = serializers.SerializerMethodField()

    class Meta:
        model = PreviousJob
        fields = ('previous_job', 'no', 'previous_position', 'previous_position_name', 'work_period', 'id')

    def get_previous_position_name(self, obj):
        return obj.previous_position.title



class WorkManDetailSerializers(serializers.ModelSerializer):
    owner_name = serializers.SerializerMethodField()
    position_name = serializers.SerializerMethodField()
    plasework_name = serializers.SerializerMethodField()
    workman_previous_job_name = serializers.SerializerMethodField()
    date_work =  serializers.DateTimeField(read_only=True, format='%d.%m.%Y')
    age = serializers.SerializerMethodField()
    work_period_workman = serializers.SerializerMethodField()

    class Meta:
        model = WorkMan
        fields = ('owner_workman', 'owner_name', 'name', 'surname', 'patronymic', 
        'dob', 'salary', 'nation', 'age', 'contact_number', 'email', 'image', 'position', 
        'position_name', 'plasework', 'plasework_name', 'date_work', 'work_period_workman', 'workman_previous_job', 'workman_previous_job_name', 'id'            
        )

    def get_work_period_workman(self, obj):
        result = datetime.now().date() - obj.date_work.date()
        return result

    def get_age(self, obj):
        result = datetime.now().date().year - obj.dob.year
        if datetime.now().date().month < obj.dob.month:
            result -= 1
        elif datetime.now().date().month == obj.dob.month:
            if datetime.now().date().day < obj.dob.day:
                result -= 1
        print(result)
        return result


    def get_owner_name(self, obj):
        return obj.owner_workman.name_owner

    def get_position_name(self, obj):
        return obj.position.title

    def get_plasework_name(self, obj):
        return[{'id':item.id, 'name':item.title}for item in obj.plasework.all()]

    def get_workman_previous_job_name(self, obj):
        instance = obj.workman_previous_job.all()
        serializer = PreviousJobSerializers(instance=instance, many=True)
        return serializer.data







