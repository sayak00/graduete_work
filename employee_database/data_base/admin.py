from django.contrib import admin
from data_base.models import Owner, Position, PlaseWork, WorkMan, PreviousJob

admin.site.register(Owner)
admin.site.register(Position)
admin.site.register(PlaseWork)
admin.site.register(WorkMan)
admin.site.register(PreviousJob)
